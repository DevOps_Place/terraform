# TERRAFORM

![featured](/uploads/4a51bf172ed024bbcd3afb1d3fe918b6/featured.png)

## Table of content 

- [What is TERRAFORM  ?](#wa)

- [Ansible vs Terraform ](#GIT)

- [Terraform Archticture ](#INST) <br>

- [Terraform configration files ](#yaml) <br>

- [Declartive vs Imperative ](#pods)<br>

- [Terraform commands for different stages](#ad)<br>

- [Standard Module Structure](#gitlab)

- [tf vs tfvars](#tf)


<a name="wa"></a>

## What is TERRAFORM ?

Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files it's an open source tool and does not required any type of license YOu can use Terrform in the below actions :

- Create Infastructure 
- Change Infastructure
- Replicate Infastructure

<a name="GIT"></a>

### Ansible vs Terraform  

In short, Terraform is an open-source, Infrastructure as Code platform, while Ansible is an open-source configuration management tool focused on the configuration of that infrastructure. It is often a topic of discussion about whether one should use Terraform or Ansible for infrastructure management.


<a name="INST"></a>

### Terraform Archtecture 

Terraform has 2 main components that make up their archtecture :

- **Core** : uses 2 input sources in order to do the job  config as a place to save what you want to create or configure  and state to keep the up to date state for infastructure  the core took these inputs and do  a plan to discover  what needs to be created/updated/destroyed by comparing the 2 sources files ( the plan created by CORE called excution plan ) .

- **Providers** : AWS |  Azure | IaaS | Kubernetes |
PaaS | SaaS  

_Through provider  you get access to resources _






<a name="yaml"></a>

### Terraform configration files

A Terraform configuration is a complete document in the Terraform language that tells Terraform how to manage a given collection of infrastructure. A configuration can consist of multiple files and directories.

**_Example_**

The following example describes a simple network topology for Amazon Web Services, just to give a sense of the overall structure and syntax of the Terraform language. Similar configurations can be created for other virtual network services, using resource types defined by other providers, and a practical network configuration will often contain additional elements not shown here.

```bash

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 1.0.4"
    }
  }
}

variable "aws_region" {}

variable "base_cidr_block" {
  description = "A /16 CIDR range definition, such as 10.1.0.0/16, that the VPC will use"
  default = "10.1.0.0/16"
}

variable "availability_zones" {
  description = "A list of availability zones in which to create subnets"
  type = list(string)
}

provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "main" {
  # Referencing the base_cidr_block variable allows the network address
  # to be changed without modifying the configuration.
  cidr_block = var.base_cidr_block
}

resource "aws_subnet" "az" {
  # Create one subnet for each given availability zone.
  count = length(var.availability_zones)

  # For each subnet, use one of the specified availability zones.
  availability_zone = var.availability_zones[count.index]

  # By referencing the aws_vpc.main object, Terraform knows that the subnet
  # must be created only after the VPC is created.
  vpc_id = aws_vpc.main.id

  # Built-in functions and operators can be used for simple transformations of
  # values, such as computing a subnet address. Here we create a /20 prefix for
  # each subnet, using consecutive addresses for each availability zone,
  # such as 10.1.16.0/20 .
  cidr_block = cidrsubnet(aws_vpc.main.cidr_block, 4, count.index+1)
}
```


<a name="pods"></a>

### Declartive vs Imperative


With an imperative tool, you define the steps to execute in order to reach the desired solution. With a declarative tool, you define the desired state of the final solution, and the automation platform determines how to achieve that state.


<a name="ad"></a>

###  Terraform commands for different stages

- **refresh** : query infastructure provider to get current state .
- **plan** : create an excution plan .
- **apply** : excute the plan .
- **destroy** : destroy the resources/infastructure .

<a name="gitlab"></a>

### Standard Module Structure:
The standard module structure is a file and directory layout we recommend for reusable modules distributed in separate repositories. Terraform tooling is built to understand the standard module structure and use that structure to generate documentation, index modules for the module registry, and more.

The standard module structure expects the layout documented below. The list may appear long, but everything is optional except for the root module. Most modules don't need to do any extra work to follow the standard structure.

**Root module**:

 This is the only required element for the standard module structure. Terraform files must exist in the root directory of the repository. This should be the primary entrypoint for the module and is expected to be opinionated. and we expect that advanced users will use specific nested modules to more carefully control what they want.


- README
- main.tf
- variables.tf
- outputs.tf

main.tf, variables.tf, outputs.tf. These are the recommended filenames for a minimal module, even if they're empty. main.tf should be the primary entrypoint. For a simple module, this may be where all the resources are created. For a complex module, resource creation may be split into multiple files but any nested module calls should be in the main file. variables.tf and outputs.tf should contain the declarations for variables and outputs, respectively.

Variables and outputs should have descriptions. All variables and outputs should have one or two sentence descriptions that explain their purpose. This is used for documentation. See the documentation for variable configuration and output configuration for more details.

```bash
$ tree minimal-module/
.
├── README.md
├── main.tf
├── variables.tf
├── outputs.tf
```
<a name="tf"></a>

### tf vs tfvars :

If your code is modularized, then every module should have “variable.tf” file with all the vars that are being used in the respective module. In this case, “terraform.tfvars” will present outside of all the modules and contains all the variables that are being used by all the modules described in “main.tf” file




